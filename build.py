import os
import shutil
from pathlib import Path

import jinja2
import yaml


def relative(target: Path, origin: Path):
    """ return path of target relative to origin """
    try:
        return Path(target).resolve().relative_to(Path(origin).resolve())
    except ValueError as e: # target does not start with origin
        # recursion with origin (eventually origin is root so try will succeed)
        return Path('..').joinpath(relative(target, Path(origin).parent))


def get_recipes(path):
    paths = sorted(path.glob('*.yaml'))
    for path in paths:
        with open(path, 'r') as f:
            recipe_dict = yaml.safe_load(f)
            yield (path.stem, recipe_dict)

# root folder for builds
website_root = Path('public/').absolute()

# homepage
homepage_output = website_root / 'index.html'

# publish the common CSS file
css_input = Path('static/style.css')
css_output = website_root / 'static' / 'style.css'

css_output.parent.mkdir(exist_ok=True, parents=True)
shutil.copy(css_input, css_output)

# prepare the Jinja templates
env = jinja2.Environment(
    loader=jinja2.FileSystemLoader("templates"),
    autoescape=jinja2.select_autoescape()
)

# create a corresponding file in the output folder for each recipe
recipe_template = env.get_template("recipe.html")

recipes_output = website_root / 'recipes/'
recipes_output.mkdir(parents=True, exist_ok=True)

# collect all recipes to be included in the homepage
recipes = {}

recipes_path = Path('recipes/').absolute()

for recipe_id, recipe in get_recipes(recipes_path):
    # collect the recipe and its publish path
    recipe_output = recipes_output / f'{recipe_id}.html'

    recipe_url = recipe_output.relative_to(website_root)
    recipes[recipe_url] = recipe

    # publish the recipe
    with open(recipe_output, 'w') as f:
        recipe_render = recipe_template.render(
            **recipe,
            css_path=relative(css_output, recipe_output.parent),
            homepage_path=relative(homepage_output, recipe_output.parent))

        f.write(recipe_render)

# create an homepage that links to all recipes
homepage_template = env.get_template("homepage.html")

with open(homepage_output, 'w') as f:
    homepage_items = [{'label': recipe['label'], 'url': url}
                      for url, recipe in recipes.items()]

    homepage_render = homepage_template.render(
        items=homepage_items,
        css_path=relative(css_output, homepage_output.parent))

    f.write(homepage_render)

# create the recipe form page
form_template = env.get_template("form.html")
form_output = website_root / 'form.html'

form_css_input = Path('static') / 'form_style.css'
form_css_output = website_root / 'static' / 'form_style.css'

form_css_output.parent.mkdir(exist_ok=True, parents=True)
shutil.copy(form_css_input, form_css_output)

form_js_input = Path('static') / 'form_script.js'
form_js_output = website_root / 'static' / 'form_script.js'

form_js_output.parent.mkdir(exist_ok=True, parents=True)
shutil.copy(form_js_input, form_js_output)

css_output.parent.mkdir(exist_ok=True, parents=True)
shutil.copy(css_input, css_output)

with open(form_output, 'w') as f:
    form_render = form_template.render(
        form_css_path=relative(form_css_output, form_output.parent),
        form_js_path=relative(form_js_output, form_output.parent))

    f.write(form_render)
