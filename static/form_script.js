// add custom submit to recipe form
function setup_form_handler() {
    var form_ = document.getElementById('recipe-form');

    if (form_.attachEvent) {
        form_.attachEvent("submit", dump_handler);
    } else {
        form_.addEventListener("submit", dump_handler);
    }
}

// convert the form to yaml, by hand
// 1) model is very simple
// 2) I want to control also the visual formatting of the YAML

function dump_handler(event) {
    // prevent refresh after form submit
    event.preventDefault();

    // load form content
    form = document.getElementById('recipe-form');
    formData = new FormData(form);
    keys = Array.from(formData.keys())

    // head
    var output = `
label: ` + formData.get('label') + `

description: ` + formData.get('description') + `

ingredients:
`;

    // load main ingredients form ids
    main_ingredient_regex = '^ingredients\\[\\\d+\\]\.label$';

    main_ingredient_ids = Array.from(formData.keys())
        .filter((label) => label.match(main_ingredient_regex, 'g'))
        .map((label) => parseInt(label.match('\\d+', 'g')));

    // populate each ingredient output
    for (var ingredient_id of main_ingredient_ids) {
        var part_id = 'ingredients[' + ingredient_id + '].label';
        output += '  - label: ' + formData.get(part_id) + '\n';

        part_id = 'ingredients[' + ingredient_id + '].comment';
        if (formData.get(part_id)) {
            output += '    comment: ' + formData.get(part_id) + '\n';
        }
    }

    // load steps form ids
    output += '\n' + 'steps:\n';

    step_regex = '^step-\\d+-label$';

    step_ids = Array.from(formData.keys())
        .filter((label) => label.match(step_regex, 'g'))
        .map((label) => parseInt(label.match('\\d+', 'g')));

    for (var step_id of step_ids) {
        // step header
        var part_id = 'step-' + step_id + '-label';
        output += '  - label: ' + formData.get(part_id) + '\n';

        part_id = 'step-' + step_id + '-duration';
        if (formData.get(part_id)) {
            output += '    duration: ' + formData.get(part_id) + '\n';
        }

        // load step ingredients form ids
        step_ingredient_regex = '^step-' + step_id + '-ingredients\\[\\d+\\].label$';

        step_ingredient_ids = Array.from(formData.keys())
            .filter((label) => label.match(step_ingredient_regex, 'g'))
            .map((label) => parseInt(label.match('\\d+.*(\\d+)')[1]));

        if (step_ingredient_ids.length > 0) {
            output += '\n' + '    ingredients:\n';
        }

        // populate each ingredient output
        for (var ingredient_id of step_ingredient_ids) {
            var part_id = 'step-' + step_id + '-ingredients[' + ingredient_id + '].label';
            output += '      - label: ' + formData.get(part_id) + '\n';

            part_id = 'step-' + step_id + '-ingredients[' + ingredient_id + '].quantity';
            if (formData.get(part_id)) {
                output += '        quantity: ' + formData.get(part_id) + '\n';
            }

            part_id = 'step-' + step_id + '-ingredients[' + ingredient_id + '].comment';
            if (formData.get(part_id)) {
                output += '        comment: ' + formData.get(part_id) + '\n';
            }

            output += '\n';
        }

        // split description into many pieces
        part_id = 'step-' + step_id + '-description';
        if (formData.get(part_id)) {
            output += '    description: >-\n';

            descriptionLines = formData.get(part_id);

            // limit description line length to ~70 characters,
            // reformatting the input
            paragraphs = descriptionLines
                .split('\n\n')
                .map((paragraph) => paragraph.replace(/\n/g,' ').trim())
                .map((paragraph) => {
                    words = paragraph.split(' ').filter((word) => word.length > 0);

                    line_chunks = [[]];
                    for (word of words) {
                        last_line_length =
                            line_chunks[line_chunks.length - 1].join(' ')
                            .length;

                        if (last_line_length + word.length < 66) {
                            line_chunks[line_chunks.length - 1].push(word);
                        }
                        else {
                            line_chunks.push([word]);
                        }
                    }

                    return line_chunks
                        .map((line) => line.join(' '))
                        .map((line) =>  '      ' + line)
                        .join('\n');
                });

            output += paragraphs.join('\n\n');
        }

        output += '\n';
    }

    // download a file with the output content
    download('recipe.yaml', output);

    return false;
}

// create a new ingredient node with required attributes
var n_ingredients = 1;
function createNewIngredientNode(baseId, n, quantityEnabled) {
    var rawNode = `
        <div class="ingredient" id="` + baseId + '-' + n + `">
            <h3>Ingrediente</h3>
            <div><input type="text" name="` + baseId + '[' + n + `].label" placeholder="Nome" required/></div>
            <div><input type="text" name="` + baseId + '[' + n + `].comment" placeholder="Commento" /></div>`;

    if (quantityEnabled) {
        rawNode += `
            <div><input type="text" name="` + baseId + '[' + n + `].quantity" placeholder="Quantità" /></div>`;
    }

    rawNode += `
            <button type="button"
                    id="` + baseId + `_deleter"
                    onclick="deleteNodeById('` + baseId + '-' + n + `')">
                Rimuovi
            </button>
        </div>`;

    var template = document.createElement('template');
    template.innerHTML = rawNode.trim();
    return template.content.firstChild;
}

// create a new step node with required outputs
var n_steps = 1;
function createNewStepNode(n) {
    // create a raw step node
    var rawNode = `
        <div class="step" id="step-` + n + `">
          <h3>Passaggio</h3>
          <div><input type="text" name="step-` + n + `-label" placeholder="Nome" required/></div>
          <div><input type="text" name="step-` + n + `-duration" placeholder="Durata" /></div>
          <div><textarea name="step-` + n + `-description" placeholder="Descrizione"></textarea></div>

          <button type="button"
                  id="step-1-ingredient-adder"
                  onclick="addIngredient('step-` + n + `-ingredients', true)">
            Aggiungi ingrediente
          </button>

          <div id="step-` + n + `-ingredients"></div>

          <button class='step-deleter'
                  type="button"
                  id="step-` + n + `_deleter"
                  onclick="deleteNodeById('step-` + n + `')">
            Rimuovi passaggio
          </button>
        </div>`;

    var template = document.createElement('template');
    template.innerHTML = rawNode.trim();
    return template.content.firstChild;
}

function deleteNodeById(id) {
    var element = document.getElementById(id);
    element.parentNode.removeChild(element);
}

// add a new ingredient node with unique progressive id
function addIngredient(baseId, quantityEnabled) {
    // create a new ingredient node
    const newNode = createNewIngredientNode(baseId, n_ingredients, quantityEnabled);
    n_ingredients += 1;

    // add it to the proper place
    document
        .getElementById(baseId)
        .appendChild(newNode)
}

// add a new step node with unique progressive id
function addStep() {
    // create a new step node
    const newNode = createNewStepNode(n_steps);
    n_steps += 1;

    // add it to the proper place
    document
        .getElementById('steps')
        .appendChild(newNode)
}

// download after form is complete
function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
