#+startup: indent
#+author: Enrico Lovisotto
#+title: Recipes website

Website for collecting recipes.

The website is generated statically from a bunch of YAML recipes in
~recipes/~ using Python & jinja2 templates.
